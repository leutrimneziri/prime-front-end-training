import { deletePost, fetchPosts, removePost, requestData } from 'reducers/posts/PostsActionsUsingMiddleware'

const defaultState = {}
const store = mockStore(defaultState)

describe('posts action creators', () => {
  // setup
  afterEach(() => { // use hook for the work you need to do repeatedly for every test
    // fetch.resetMocks()
    store.clearActions() // clear the stored actions
  })

  test('should create RECEIVE_POSTS after a successful fetch', () => {

    const data = []

    fetch.mockResponseOnce(JSON.stringify(data))

    const expectedActions = [
      {type: 'REQUEST_POSTS'},
      {type: 'RECEIVE_POSTS', data}
    ]

    store.dispatch(fetchPosts()).then(() => {
      const actions = store.getActions() // the actions of the mock store
      expect(actions).toEqual(expectedActions)
    })
  })

  it('should create DISPLAY_MESSAGE_POSTS after a failed fetch', () => {
    const message = 'Failed to fetch'
    const response = new Error(message)
    fetch.mockRejectOnce(response)
    const expectedActions = [
      {type: 'REQUEST_POSTS'},
      {type: 'DISPLAY_MESSAGE_POSTS', response}
    ]
    store.dispatch(fetchPosts()).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })

  it('should create DELETE_POST after a successful deletion', () => {
    const item = {id: '1', name: 'Post to delete'}
    fetch.mockResponseOnce(JSON.stringify(item))
    const expectedActions = [
      requestData(),
      removePost(item)
    ]
    store.dispatch(deletePost(item)).then(() => {
      expect(store.getActions()).toEqual(expectedActions)
    })
  })
})