import postsReducer from 'reducers/posts/Posts'
import { receiveData, updatePost } from 'reducers/posts/PostsActions'
import ACTION_TYPES from 'reducers/posts/PostsActionTypes'

describe('posts reducer', () => {

  const defaultState = {
    items: [],
    request: {isLoading: false, status: 200},
    filter: ''
  }

  it('should return the initial state', () => {
    expect(postsReducer(undefined, {})).toEqual(defaultState)
  })

  it('should enter loading state', () => {
    expect(
      postsReducer(defaultState, {type: ACTION_TYPES.REQUEST_DATA})
    ).toEqual({
      items: [],
      request: {isLoading: true, status: 200},
      filter: ''
    })
  })

  it('should receive data', () => {
    expect(
      postsReducer(defaultState, receiveData([]))
    ).toEqual(defaultState)
  })

  it('should update post', () => {
    expect(
      postsReducer(defaultState, updatePost({id: '1', title: 'Test post'}))
    ).toEqual({
      items: [{id: '1', title: 'Test post'}],
      request: { isLoading: false, status: 200 },
      filter: ''
    })
  })

  it('should remove post', () => {
    expect(
      postsReducer({
        items: [{id: '1', title: 'Test post'}],
        request: { isLoading: false, status: 200 },
        filter: ''
      }, {type: 'DELETE_POST', item: {id: '1', title: 'Test post'}})
    ).toEqual(defaultState)
  })
})
