/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import React, { Fragment } from 'react'
import { shallow, mount, render } from 'enzyme'
import { expect as expectChai } from 'chai'
import routes from 'utils/Routes'
import { PAGES } from 'Constants'
import Divider from 'presentations/Divider';
import TestingReactComponents from "pages/lecture12/TestingReactComponents";
import { TestingReactComponents as TestingReactComponentsStripped } from "pages/lecture12/TestingReactComponents";
import ThemeProvider from 'utils/ThemeProvider'

test('should be able to at least render first tree!', () => {
  const section = routes.find(next => next.id === PAGES.LECTURE_12.ID)
  // Render the testing react components page, store is mocked to be only {}
  const wrapper = shallow(<TestingReactComponentsStripped section={section}/>)
  // Here is how you can explore the subtree of this component
  expectChai(wrapper.exists(Divider)).to.be.true // same as expect(wrapper.exists(Divider)).true
  expect(wrapper.exists(Divider)).toBe(true)

  expectChai(wrapper.find(Divider)).to.have.lengthOf(1)
  expect(wrapper.find(Divider)).toHaveLength(1)

  expectChai(wrapper.find({variant: 'heading'})).to.have.lengthOf(1)
  expect(wrapper.find({variant: 'heading'})).toHaveLength(1)

  expectChai(wrapper.find('#testing').prop('variant')).to.eql('title')
  expect(wrapper.find('#testing').props()).toMatchObject({variant: 'title'})
})


test('should be able to mount itself and the entire tree of components', () => {
  const section = routes.find(next => next.id === PAGES.LECTURE_12.ID)
  // Render the testing react components page
  const wrapper = mount(<ThemeProvider><TestingReactComponents store={mockStore({})} section={section} /></ThemeProvider>)

  expectChai(wrapper.find(Divider)).to.have.lengthOf(1)
  expect(wrapper.find(Divider)).toHaveLength(1)
})

const Simple = (props) => {
  return (
    <div>
      Simple component
      <ol>
        <li className="first">First</li>
        <li id="next">Second</li>
      </ol>
    </div>
  )
}


test('should find text in nodes', () => {

  // Render the testing react components page
  const wrapper = render(<Simple/>)

  expectChai(wrapper.find(`.first`).text()).to.eql('First')
  expectChai(wrapper.find(`ol`).html()).to.contain('li')
  expectChai(wrapper.find(`#next`).is('li')).to.be.true
})

test('Should be able statically render itself, here we are only interested in rendered HTML', () => {
  const section = routes.find(next => next.id === PAGES.LECTURE_12.ID)
  // Render the testing react components page
  const wrapper = render(<ThemeProvider><TestingReactComponents store={mockStore({})} section={section} /></ThemeProvider>)
  expect(wrapper.find(`#divider`)).toBeTruthy()
})