/**
 * Created by LeutrimNeziri on 09/04/2019.
 */
import withStyles from "@material-ui/core/styles/withStyles";
import Divider from "presentations/Divider";
import PageLink from "presentations/rows/nav/PageLink";
import Typography from "presentations/Typography";
import Code from 'presentations/Code';
import React, { Fragment } from "react";
import htmlMarkup from 'assets/images/lecture7/html_markup.png'
import cssCode from 'assets/images/lecture7/css_code.png'
const styles = ({ typography }) => ({
  root: {},
},{name:"--0s-0s-00s"})

const divWithInlineStyle = `
<div style={{ backgroundColor: 'red' }}>
  Hello World!
</div>
`
const css_1 = `
body #container .main ul li:nth-child(2n) div p ~ [type="text"]{}
    body #container .main .wrapper *{}
    input#firstName{}
    div.dropdown::before{}
    a.button:hover{}`

class CSSRulesAndSelectors extends React.Component {
  render() {
    const { classes, section } = this.props
    return (
      <Fragment>
        <Typography variant={'heading'}>
          {section.display}
          <Divider />
        </Typography>
        <Typography variant={'h3'}>
          CSS Rules
        </Typography>

        <Typography>
          Specificity is a rule used by browsers to apply different priorities to different CSS selectors. Therefore styles
          applied using high priority CSS selector cannot be overridden by a low priority CSS selector.
          <a href="http://qnimate.com/dive-into-css-specificity/" target="_blank">
            <img src="http://qnimate.com/wp-content/uploads/2014/07/specificity-table1.jpg" alt="specificity rules" />
          </a>
        </Typography>

        <Typography variant={'h3'}>
          What Is CSS Specificity?
        </Typography>
        <Typography>
          Specificity is a rule used by browsers to apply different priorities to different CSS selectors. Therefore styles
          applied using high priority CSS selector cannot be overridden by a low priority CSS selector.
        </Typography>


        <ul>
          <li><strong>Universal Selectors:</strong> Universal selectors are *, ~, &gt;, + and [space]</li>
          <li><strong>Type selectors:</strong> Type selectors are used to select elements based on their tag name. For
            example:{` span{font-size: 12;}`}</li>
          <li><strong>Class Selectors:</strong> Class selector are used to select elements based on their class. For example:
            {`.footer{font-size: 100px;}`}</li>
          <li><strong>Attribute selectors:</strong> Attribute selectors are used to select elements based in their attribute.
            For example: input[type=”text”]</li>
          <li><strong>Pseudo-Classes and Pseudo-Element:</strong> Pseudo-class selectors select elements based on their state.
            For example: {`a:hover{color: blue;}`}. Pseudo elements target specific parts of a HTML element instead of the
            complete HTML element. For example: p::first-line{}</li>
          <li><strong>ID Selector:</strong> Id selector is used to select elements based on their id. For example:
            {`#elementId{color: orange;}`}</li>
          <li><strong>Inline Style:</strong> We can put CSS code inside the style attribute of a HTML element.</li>
        </ul>

        <Code>
          {css_1}
        </Code>

        <Typography variant={'h3'}>
          Exercise
        </Typography>
        <img src={htmlMarkup} style={{width: '50%'}} alt="html markup" />
        <img src={cssCode} style={{width: '50%'}}  alt="css CODE" />
      </Fragment>
    )
  }
}

export default withStyles(styles)(CSSRulesAndSelectors)
